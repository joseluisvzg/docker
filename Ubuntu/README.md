Install Docker on the host machine:
$ apt-get install docker.io

Enable & Start docker service on the host machine:
$ systemctl start docker
$ systemctl enable docker

*clone repository

build image:
$ docker build -t ubuntu_image .

create container:
$ docker run -p 2222:22 -p 21:21 ubuntu_image

add user:
$ adduser --shell /bin/ftponly test

