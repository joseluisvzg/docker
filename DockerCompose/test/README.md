Alternative composer installation (image):
docker pull docker/compose
Run composer (image):
docker run docker/compose

Check file validity:
docker-compose config

Start:
docker-compose up -d

Stop:
docket-compose down

docker-compose ps

Scale:
docker-compose up -d --scale database=3
